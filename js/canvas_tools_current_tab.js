// ==UserScript==
// @name        Open all canvas tools in current tab
// @namespace   Violentmonkey Scripts
// @match       https://utexas.instructure.com/courses/*
// @grant       none
// @version     1.0
// @author      Daniel Parks
// @description 4/24/2022, 9:10:01 PM
// @run-at      document-idle
// @downloadURL https://gitlab.com/danielrparks/ut-userscripts/-/raw/main/js/canvas_tools_current_tab.js
// @noframes
// ==/UserScript==

{
  let b = document.querySelector("form#tool_form > div > div.load_tab > div > button");
  if (b) {
    b.parentElement.parentElement.parentElement.parentElement.dataset.toolLaunchType = "self";
  }
}
