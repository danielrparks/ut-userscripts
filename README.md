# My UT userscripts
These are the userscripts I've written to make the UX on UT's websites nicer.

I test on Firefox using [Violentmonkey](https://violentmonkey.github.io), but they *should* work on any browser using any Greasemonkey variant. If you find bugs, please report them on the issues page.

You can add them from the main Violentmonkey page using the "install from URL" option.

# Bookmarklets
I also have some [bookmarklets](https://danielrparks.gitlab.io/ut-userscripts/)
